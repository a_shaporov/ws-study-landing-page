const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

const requireDir = require('require-dir');
const tasks = requireDir('./tasks');

exports.scss = tasks.scss;
exports.html = tasks.html;
exports.srcfiles = tasks.srcfiles;
exports.images = tasks.images;
exports.types = tasks.types;

exports.default = gulp.parallel(
    exports.scss,
    exports.html,
    exports.images,
    exports.types,
)