const {
    src,
    dest
} = require('gulp');

module.exports = function images() {
    return src('src/types/**/*')
        .pipe(dest('build/src/types'))
}