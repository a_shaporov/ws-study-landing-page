const {
    src,
    dest
} = require('gulp');

module.exports = function images() {
    return src('src/img/**/*')
        .pipe(dest('build/src/img'))
}