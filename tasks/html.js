const {
    src,
    dest
} = require('gulp');

const include = require('gulp-file-include');
const bs = require('browser-sync');

module.exports = function html() {
    return src('./index.html')
        .pipe(dest('build'))
    .pipe(bs.stream())
}